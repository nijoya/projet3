export interface Rank {
    username: string
    score: number
}

export interface RankClient {
    username: string
    score: number
    pos: number 
}