export default interface PrivateProfile {
    username : string /* Used as key value */
    firstname : string
    lastname : string
    password : string
    avatar : string /* String for the moment eventually needs to be image */
    rooms_joined: string[]
}