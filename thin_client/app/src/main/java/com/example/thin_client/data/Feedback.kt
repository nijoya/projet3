package com.example.thin_client.data


data class Feedback(
    val status: Boolean,
    val log_message: String
)