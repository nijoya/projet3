package com.example.thin_client.data.drawing

data class DrawingAttributes(
    val Color: String,
    val Width: Number,
    val StylusTip: Shape,
    val Top: Number
)