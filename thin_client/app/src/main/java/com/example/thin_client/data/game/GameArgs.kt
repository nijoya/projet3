package com.example.thin_client.data.game

object GameArgs {
    const val IS_GAME_CHAT = "isGameChat"
    const val CHOSEN_WORD = "chosenWord"
}