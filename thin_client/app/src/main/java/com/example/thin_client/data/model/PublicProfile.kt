package com.example.thin_client.data.model

data class PublicProfile (
    val username: String,
    val avatar: String
)