package com.example.thin_client.data.model

data class Rank (
    val username: String,
    val score:Number
)