package com.example.thin_client.data

data class ClientMessage(
    val content: String,
    val roomId: String
)
