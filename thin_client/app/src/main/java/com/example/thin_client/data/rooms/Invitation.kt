package com.example.thin_client.data.rooms

data class Invitation(
    val id: String,
    val username: String
)