package com.example.thin_client.data.drawing

data class StylusPoint(
    val X: Number,
    val Y: Number
)