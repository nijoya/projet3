package com.example.thin_client.data.game

enum class MatchMode {
    FREE_FOR_ALL,
    SOLO,
    COLLABORATIVE,
    ONE_ON_ONE,
}