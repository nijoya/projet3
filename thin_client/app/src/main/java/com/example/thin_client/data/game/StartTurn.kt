package com.example.thin_client.data.game

data class StartTurn(
    val timeLimit: Number,
    val word: String
)