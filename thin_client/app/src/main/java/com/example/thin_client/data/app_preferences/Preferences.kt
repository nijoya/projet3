package com.example.thin_client.data.app_preferences

object Preferences {
    const val USER_PREFS = "USER_PREFERENCES"
    const val LOGGED_IN_KEY = "user_logged_in"
    const val USERNAME = "username"
    const val PASSWORD = "password"
    const val AVATAR = "avatar"
    const val FIRST_NAME = "first_name"
    const val LAST_NAME = "last_name"
}