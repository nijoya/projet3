package com.example.thin_client.data.server

object HTTPRequest {
    const val BASE_URL = "http://35.203.24.149:5000"
    const val URL_CREATE = "/profile/create"
    const val URL_PRIVATE = "/profile/private/"
    const val URL_PROFILE = "/profile/"
    const val URL_RANKING = "/profile/rank/"
    const val URL_STATS = "/profile/stats/"
}