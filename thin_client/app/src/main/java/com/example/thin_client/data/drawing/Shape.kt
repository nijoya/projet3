package com.example.thin_client.data.drawing

enum class Shape {
    RECTANGLE,
    ELLIPSE
}