package com.example.thin_client.data.lifecycle

enum class LoginState {
    FIRST_LOGIN,
    LOGIN_WITH_EXISTING,
    LOGGED_IN
}