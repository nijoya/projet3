package com.example.thin_client.data.game

data class Score(
    val username: String,
    val updateScore: UpdateScore
)