package com.example.thin_client.data.game

data class UpdateScore(
    val scoreTotal: Number,
    val scoreTurn: Number
)