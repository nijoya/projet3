package com.example.thin_client.data.model

data class RankClient(
    var username: String,
    var score: Number,
    var pos: Number
)