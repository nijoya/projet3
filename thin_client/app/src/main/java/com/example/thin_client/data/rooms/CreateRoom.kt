package com.example.thin_client.data.rooms

data class CreateRoom(
    val id: String,
    val isPrivate: Boolean
)